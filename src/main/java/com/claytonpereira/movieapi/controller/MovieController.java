package com.claytonpereira.movieapi.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.claytonpereira.movieapi.model.Movie;
import com.claytonpereira.movieapi.model.User;
import com.claytonpereira.movieapi.repository.MovieRepository;


@RestController
@RequestMapping("/movie")

public class MovieController {
	
@Autowired
private final MovieRepository movieRepository;
Logger logger = LoggerFactory.getLogger(UserController.class);

MovieController(MovieRepository movieRepository) {
    this.movieRepository = movieRepository;
  }

	@PostMapping("/register")
	public Movie saveMovie(@RequestBody Movie newMovie) {
        logger.info("An INFO Message");
		return movieRepository.save(newMovie);
	}
	@GetMapping("/list")
	public List<Movie> listMovies() {
		List<Movie> list = (List<Movie>) movieRepository.findAll();
		return list;
		
	}
}

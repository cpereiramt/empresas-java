package com.claytonpereira.movieapi.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.claytonpereira.movieapi.model.Movie;
import com.claytonpereira.movieapi.model.UserVote;

import com.claytonpereira.movieapi.repository.UseVoteRepository;

@RestController
@RequestMapping("/vote")

public class VoteController {

	@Autowired
	private final UseVoteRepository voteRepository;
	
	


	Logger logger = LoggerFactory.getLogger(VoteController.class);

	VoteController(UseVoteRepository voteRepository) {
	    this.voteRepository = voteRepository;

	  }
	
	
	@PostMapping("/register")
   @Transactional
	public UserVote saveVote(@RequestBody UserVote newVote) {
		
		voteRepository.save(newVote);
        return newVote;
		

	}
	
	@GetMapping("/list")
	public List<UserVote> listMovies() {
		List<UserVote> list = (List<UserVote>) voteRepository.findAll();
		return list;
		
	}
}

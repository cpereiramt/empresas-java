package com.claytonpereira.movieapi.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.claytonpereira.movieapi.model.User;
import com.claytonpereira.movieapi.repository.UserRepository;

@RestController
@RequestMapping("/user")
public class UserController {
	
@Autowired
private final UserRepository userRepository;
Logger logger = LoggerFactory.getLogger(UserController.class);

UserController(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

	@PostMapping("/register")
	public User saveUser(@RequestBody User newUser) {
        logger.info("An INFO Message");
		return userRepository.save(newUser);
	}
	
	@PostMapping("/activate/{id}")

	public void activateUser(@PathVariable Long id) {
		
		 userRepository.activateUser(id);
	}
	
	@PostMapping("/inactivate/{id}")

	public void InactivateUser(@PathVariable Long id) {
		
		 userRepository.InativateUser(id);
	}
	
	@PostMapping("/updateusername/{id}")
	public User UpdateUser(@PathVariable Long id, @RequestBody User user) {
		User userFound = userRepository.findById(id);
		userFound.setUsername(user.getUsername());
		return userRepository.save(userFound);
	}
	
	@GetMapping("/list")
	public List<User> listActiveUsers() {
		List<User> user = userRepository.findByIsActive(true);
		return user;
		
	}
}

package com.claytonpereira.movieapi.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.claytonpereira.movieapi.model.User;



public interface UserRepository extends CrudRepository<User,String> {
    User findByUsername(String username);
    User findById(Long id);
    
    @Query("select u from User u where u.IsActive like :isActive and u.role like 'USUARIO'")
    List<User> findByIsActive(Boolean isActive);
    
    @Modifying
    @Query("update User u set u.IsActive = false where u.id = :id")
	@Transactional
    void InativateUser(@Param(value = "id") long id);
    
    @Modifying
    @Query("update User u set u.IsActive = true where u.id = :id")
	@Transactional
    void activateUser(@Param(value = "id") long id);
}

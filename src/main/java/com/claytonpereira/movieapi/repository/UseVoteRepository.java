package com.claytonpereira.movieapi.repository;


import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.claytonpereira.movieapi.model.UserVote;

public interface UseVoteRepository extends CrudRepository<UserVote,String> {
    UserVote findById(Long id);
    
    @Modifying
    @Query("update User u set u.IsActive = false where u.id = :id")
	@Transactional
    void Activate(@Param(value = "id") long id);
    @Modifying
    @Query("update User u set u.IsActive = false where u.id = :id")
	@Transactional
    void InativateUser(@Param(value = "id") long id);
    
}

package com.claytonpereira.movieapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.claytonpereira.movieapi.model.Movie;

public interface MovieRepository extends CrudRepository<Movie,String> {
    Movie findByName(String name);
}

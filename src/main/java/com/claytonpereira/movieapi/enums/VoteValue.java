package com.claytonpereira.movieapi.enums;

public enum VoteValue {
    NÃO_GOSTOU,
    GOSTOU_POUCO,
    GOSTOU_PARCIALMENTE,
    GOSTOU,
    GOSTOU_MUITO 
    
}

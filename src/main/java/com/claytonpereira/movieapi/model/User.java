package com.claytonpereira.movieapi.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.claytonpereira.movieapi.enums.Role;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@DynamicUpdate
@Table(name= "USER")
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1711183189638516949L;

	@Id @GeneratedValue
	private long id;
	
	@Column(name="USERNAME", nullable = false, unique = true)
	private String username;
	
	

	@Column(name="EMAIL", nullable = false, unique = true)
	private String email;
	
	@Column(name="PASSWORD", nullable = false)
	private String password;
	
	@Column(name="FIRSTNAME", nullable = false)
	private String firstName;
		
	@Column(name="LASTNAME", nullable = false)
	private String lastName;
	
	@Enumerated(EnumType.STRING)
    private Role role;
	
    
    @Column(name="ISACTIVE", nullable = false)
    private Boolean IsActive;
    public Set<UserVote> getUserVotes() {
		return userVotes;
	}

	public void setUserVotes(Set<UserVote> userVotes) {
		this.userVotes = userVotes;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	   @Column(nullable = false)
	 //@JsonManagedReference
	@JsonManagedReference(value="user-vote")    
	private Set<UserVote> userVotes;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
    
	  public Boolean getIsActive() {
			return IsActive;
		}

		public void setIsActive(Boolean isActive) {
			IsActive = isActive;
		}
		
		

}

package com.claytonpereira.movieapi.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name= "ACTOR")
public class Actor implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4025566784144140261L;


	@Id @GeneratedValue
	private long id;
	
	
	@Column(name="NAME", nullable = false, unique = true)
	private String name;	
//	
//	 @ManyToMany(fetch = FetchType.LAZY,
//	            cascade = {
//	                CascadeType.PERSIST,
//	                CascadeType.MERGE
//	            },
//	            mappedBy = "ACTOR")
//		private Set <Movie> movie;
//	 
	 
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



}

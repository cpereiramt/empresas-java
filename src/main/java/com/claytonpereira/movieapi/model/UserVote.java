package com.claytonpereira.movieapi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicUpdate;

import com.claytonpereira.movieapi.enums.VoteValue;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@DynamicUpdate
public class UserVote implements Serializable {
	   /**
	 * 
	 */
	private static final long serialVersionUID = 886205145153198735L;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


//	public Vote getVote() {
//		return vote;
//	}
//
//
//	public void setVote(Vote vote) {
//		this.vote = vote;
//	}

//	
//	public Movie getMovie() {
//		return movie;
//	}
//
//
//	public void setMovie(Movie movie) {
//		this.movie = movie;
//	}


	@Id
	    @GeneratedValue
	    private long id;

	    @ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "user_id")
	   // @JsonManagedReference
		  @JsonBackReference(value="user-vote")
	    private User user;

//	    @ManyToOne(fetch = FetchType.LAZY, optional = true)
//	    @JoinColumn(name = "vote_id")
//	    private Vote vote;
	    

	    @ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "movie_id")
		  @JsonBackReference(value="movie-vote")
	    private Movie movie;
	    

		    @Enumerated(EnumType.STRING)
		private VoteValue voteValue;


		public VoteValue getVoteValue() {
			return voteValue;
		}


		public void setVoteValue(VoteValue voteValue) {
			this.voteValue = voteValue;
		}

}

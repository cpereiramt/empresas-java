package com.claytonpereira.movieapi.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name= "MOVIE")
public class Movie implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3301593365202474485L;
	@Id @GeneratedValue
	private long id;
	private String name;
	private String genre;
	@ManyToMany(fetch= FetchType.LAZY,
			cascade = { 
					CascadeType.PERSIST,
					CascadeType.MERGE
			})
	@JoinTable(
			name = "movie_directors",
			joinColumns =  { @JoinColumn(name = "movie_id")},
		    inverseJoinColumns = { @JoinColumn(name = "director_id")})
	
	private Set <Director> directors;
	
	@ManyToMany(fetch= FetchType.LAZY,
			cascade = { 
					CascadeType.PERSIST,
					CascadeType.MERGE
			})
	@JoinTable(
			name = "movie_actors",
			joinColumns =  { @JoinColumn(name = "movie_id")},
		    inverseJoinColumns = { @JoinColumn(name = "actor_id")})	
	private Set <Actor> actors; 	
    public Set<UserVote> getUserVotes() {
		return userVotes;
	}
	public void setUserVotes(Set<UserVote> userVotes) {
		this.userVotes = userVotes;
	}
	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
	
	@JsonManagedReference(value="movie-vote")
    private Set<UserVote> userVotes;

	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Set<Director> getDirectors() {
		return directors;
	}
	public void setDirectors(Set<Director> directors) {
		this.directors = directors;
	}
	public Set<Actor> getActors() {
		return actors;
	}
	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}
	
}
